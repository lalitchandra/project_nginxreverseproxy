provider aws {}

resource "aws_ecr_repository" "nginx" {
name = "nginx"
}

output "Registry URL" {
value ="${aws_ecr_repository.nginx.repository_url}"
}
