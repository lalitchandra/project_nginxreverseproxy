# Internet VPC

resource "aws_vpc" "vpc-tf" {
  cidr_block           = "172.21.0.0/16"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  enable_classiclink   = "false"

  tags = {
    Name = "vpc-terraform"
  }
}
#Public subnet in different zone "us-west-2b"
resource "aws_subnet" "vpc-public-1" {
  vpc_id                  = "${aws_vpc.vpc-tf.id}"
  cidr_block              = "172.21.10.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-west-2b"

  tags = {
    Name = "vpc-public-1"
  }
}
#Private subnet in different zone "us-west-2a"
resource "aws_subnet" "vpc-private-1" {
  vpc_id                  = "${aws_vpc.vpc-tf.id}"
  cidr_block              = "172.21.20.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "us-west-2a"

  tags = {
    Name = "vpc-private-1"
 }
}
#Internet gateway for the public subnet
resource "aws_internet_gateway" "igw" {
 vpc_id                  = "${aws_vpc.vpc-tf.id}"
 tags = {
  name = "igw"
 }
}
#Elastic ip for NAT
resource "aws_eip" "nat_eip" {
 vpc         = true
  depends_on = ["aws_internet_gateway.igw"]
}
#NAT_GATEWAY
resource "aws_nat_gateway" "nat" {
  allocation_id = "${aws_eip.nat_eip.id}"
  subnet_id     = "${aws_subnet.vpc-public-1.id}"
  depends_on = ["aws_internet_gateway.igw"]
tags = {
  name = "gw NAT"
 }
}
#Routing table for private subnet
resource "aws_route_table" "private" {
 vpc_id     = "${aws_vpc.vpc-tf.id}"

tags = {
 name = "private"
 }
}
#Routing table for public subnet
resource "aws_route_table" "public" {
 vpc_id  = "${aws_vpc.vpc-tf.id}"
 tags = {
  name = "public"
 }
}
resource "aws_route" "public_internet_gateway" {
  route_table_id  = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.igw.id}"
}
resource "aws_route" "private_nat_gateway" {
 route_table_id  = "${aws_route_table.private.id}"
 destination_cidr_block = "0.0.0.0/0"
 nat_gateway_id  = "${aws_nat_gateway.nat.id}"
}
#Route table associations
resource "aws_route_table_association" "public" {
 subnet_id  = "${aws_subnet.vpc-public-1.id}"
 route_table_id = "${aws_route_table.public.id}"
}
resource "aws_route_table_association" "private" {
 subnet_id  = "${aws_subnet.vpc-private-1.id}"
 route_table_id = "${aws_route_table.private.id}"
}




